# Installation

There are three ways to install *{{ cookiecutter.project_name }}* on your machine. Via a manual binary download, an install script, or installation of the python package.

## Binaries

To install `{{ cookiecutter.project_name }}`, download the appropriate binary from one of the links below, and set the downloaded file to be executable (``chmod +x {{ cookiecutter.project_name }}``):

  - [Linux](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/{{ cookiecutter.project_name }})
  - [Windows](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/windows/{{ cookiecutter.project_name }}.exe)
  - [Mac OS X](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/darwin/{{ cookiecutter.project_name }})


## Install script  

Alternatively, use the 'curly' install script for `{{ cookiecutter.project_name }}`:

``` console
curl https://gitlab.com/frkl/{{ cookiecutter.project_name }}/-/raw/develop/scripts/install/{{ cookiecutter.project_name }}.sh | bash
```


This will add a section to your shell init file to add the install location (``$HOME/.local/share/frkl/bin``) to your ``$PATH``.  

You might need to source that file (or log out and re-log in to your session) in order to be able to use *{{ cookiecutter.project_name }}*:

``` console
source ~/.profile
```


## Python package

The python package is currently not available on [pypi](https://pypi.org), so you need to specify the ``--extra-url`` parameter for your pip command. If you chooose this install method, I assume you know how to install Python packages manually, which is why I only show you an example way of getting *{{ cookiecutter.project_name }}* onto your machine:

``` console
> python3 -m venv ~/.venvs/{{ cookiecutter.project_name }}
> source ~/.venvs/{{ cookiecutter.project_name }}/bin/activate
> pip install --extra-index-url https://pkgs.frkl.io/frkl/dev {{ cookiecutter.project_name }}
Looking in indexes: https://pypi.org/simple, https://pkgs.frkl.io/frkl/dev
Collecting {{ cookiecutter.project_name }}
  Downloading http://pkgs.frkl.io/frkl/dev/%2Bf/ee3/f57bd91a076f9/{{ cookiecutter.project_name }}-0.1.dev24%2Bgd3c4447-py2.py3-none-any.whl (28 kB)
...
...
...
Successfully installed aiokafka-0.6.0 aiopg-1.0.0 ... ... ...
> {{ cookiecutter.project_name }} --help
Usage: {{ cookiecutter.project_name }} [OPTIONS] COMMAND [ARGS]...
   ...
   ...
```

# Description

A Python project template.

# Requirements

- [cruft](https://github.com/timothycrosley/cruft)
- [direnv](https://direnv.net/) (optional)


# Quickstart

    # create Python project from this template (answer questions interactively):
    
    > cruft create https://gitlab.com/frkl/template-python-project.git

    # if not using [direnv](https://direnv.net), you have to setup and activate your
    # Python virtualenv yourself, manually. Otherwise, 'cd' into the new project directory
    # and 'allow' the new '.envrc' file:
    
    > direnv allow
    # now initialize the new project (install project & dev requirements & setup other dev related things):
    > make init
    > git commit -m "chore: initial commit"
        
    
# ``make`` targets

- ``init``: init development project (install project & dev dependencies & pre-commit hook)
- ``binary``: create binary for project (will install *pyenv* -- check ``scripts/build-binary`` for details)
- ``flake``: run *flake8* tests
- ``mypy``: run mypy tests
- ``test``: run unit tests
- ``docs``: create static documentation pages
- ``serve-docs``: serve documentation pages (incl. auto-reload)
- ``clean``: clean build directories

For details (and other, minor targets), check the ``Makefile``.
    
## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2020 frkl OÜ](https://frkl.io)
